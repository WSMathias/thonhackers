
package hackathon;

public class Terminal {
    
    private static void printArray(String[] anArray) {
      for (int i = 0; i < anArray.length; i++) {
         
          if (i == 0) {
            System.out.println("");
         }
         
         System.out.println(i+"=>"+anArray[i]);
      }
   }
    
}
