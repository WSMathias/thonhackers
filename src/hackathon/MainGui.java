package hackathon;

import java.io.BufferedReader;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingWorker;

public class MainGui extends javax.swing.JFrame {

    

//variables for backup tab
public static String[] hddList=new String[10];
public String srcHdd="";
public String dstDir="";
public String srcDir="";
public boolean pbf=true;
public String rootCustom="";

//variables for restore tab
public String srcImg="";
public String imgDstHdd="";
private SimulatedActivity activity;
public static String rootPass= "";
ShellScript s=new ShellScript();


    public MainGui() {
        initComponents();
//        JFrame frame = new ProgressBarFrame();
//              frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//               frame.setVisible(true);
        disableAll();
        //Mounter mounter=new Mounter();
//        customDir.setEnabled(false);
//        customDir.setEditable(false);
//        BrowseCustom.setEnabled(false);
//        destinationDir.setEnabled(false);
//        destinationDir.setEditable(false);
//        browseDestination.setEnabled(false);
//        startBackup.setEnabled(false);
//        //restore tab
//        startRestore.setEnabled(false);
//        imageSource.setEditable(false);
        
    }
    

    private static void printArray(String[] anArray) {
      for (int i = 0; i < anArray.length; i++) {
         
          if (i == 0) {
            System.out.println("");
         }
         
         System.out.println(i+"=>"+anArray[i]);
      }
   }
    
    private  String getHomeDir(){
        String dir = s.runCmd("whoami");
        //System.out.println(dir);
        return "/home/";//+dir;
    }
    
    private void disableAll(){
        customDir.setEnabled(false);
        customDir.setEditable(false);
        customDir.setText(null);
        fullBackup.setSelected(false);
        customBackup.setSelected(false);
        BrowseCustomDir.setEnabled(false);
        destinationDir.setEnabled(false);
        destinationDir.setText(null);
        destinationDir.setEditable(false);
        browseDestinationDir.setEnabled(false);
        startBackup.setEnabled(false);
        //restore tab
        startRestore.setEnabled(false);
        imageSource.setEditable(false);
        imageSource.setText(null);
    }
    
    
    private  String[] loadDisks(){
        s.bashScript("umount /mnt");
        return s.runCmd("lsblk -So NAME,TRAN,VENDOR,MODEL",1);
    }
    
    public String fileSelector(boolean type,String dir){
        
        JFileChooser fileChooser = new JFileChooser();
        
        if(dir!=null){
            fileChooser = new JFileChooser(dir);
        }

        //For Directory
        if(type){
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        }
        // For File
        if(!type){
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        }
        fileChooser.setAcceptAllFileFilterUsed(false);

        int rVal = fileChooser.showOpenDialog(null);
        if (rVal == JFileChooser.APPROVE_OPTION) {
            
            System.out.println(fileChooser.getSelectedFile().toString());
            return fileChooser.getSelectedFile().toString();
        }
        return null;
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jDesktopPane2 = new javax.swing.JDesktopPane();
        jPanel4 = new javax.swing.JPanel();
        jProgressBar = new javax.swing.JProgressBar();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        customBackup = new javax.swing.JRadioButton();
        BrowseCustomDir = new javax.swing.JButton();
        startBackup = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        fullBackup = new javax.swing.JRadioButton();
        customDir = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        hdd = new javax.swing.JComboBox<>();
        browseDestinationDir = new javax.swing.JButton();
        destinationDir = new javax.swing.JTextField();
        reloadHdd = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        srcHddRestore = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        imageSource = new javax.swing.JTextField();
        browseImageSrc = new javax.swing.JButton();
        startRestore = new javax.swing.JButton();
        reloadRestore = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        repair = new javax.swing.JButton();
        comboBoxBootRepair = new javax.swing.JComboBox<>();
        reloadHddBootRepair = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Root Backup");
        setForeground(java.awt.Color.orange);
        setResizable(false);

        jTabbedPane1.setBackground(new java.awt.Color(254, 159, 33));

        jPanel1.setBorder(new javax.swing.border.MatteBorder(null));

        customBackup.setText("Custom Backup");
        customBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customBackupActionPerformed(evt);
            }
        });

        BrowseCustomDir.setText("...");
        BrowseCustomDir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BrowseCustomDirActionPerformed(evt);
            }
        });

        startBackup.setText("Start");
        startBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startBackupActionPerformed(evt);
            }
        });

        jLabel1.setText("Select HDD :");

        jLabel3.setText("Select Destination :");

        fullBackup.setText("Full Backup");
        fullBackup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fullBackupActionPerformed(evt);
            }
        });

        customDir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                customDirActionPerformed(evt);
            }
        });

        jLabel2.setText("Select Directory :");

        hdd.setModel(new javax.swing.DefaultComboBoxModel<>(loadDisks()));
        hdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hddActionPerformed(evt);
            }
        });

        browseDestinationDir.setText("...");
        browseDestinationDir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseDestinationDirActionPerformed(evt);
            }
        });

        destinationDir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                destinationDirActionPerformed(evt);
            }
        });

        reloadHdd.setText("\u21bb");
        reloadHdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reloadHddActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(destinationDir, javax.swing.GroupLayout.PREFERRED_SIZE, 392, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(browseDestinationDir))
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(hdd, javax.swing.GroupLayout.PREFERRED_SIZE, 376, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(reloadHdd, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(fullBackup)
                                    .addGap(151, 151, 151)
                                    .addComponent(customBackup))
                                .addComponent(customDir, javax.swing.GroupLayout.PREFERRED_SIZE, 392, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(BrowseCustomDir))))
                .addContainerGap(92, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(startBackup, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(258, 258, 258))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(hdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(reloadHdd))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(customBackup)
                            .addComponent(fullBackup))
                        .addGap(48, 48, 48))
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(customDir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BrowseCustomDir))
                .addGap(49, 49, 49)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(destinationDir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(browseDestinationDir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(startBackup)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Backup", jPanel1);

        jLabel4.setText("Select Backup Image :");

        srcHddRestore.setModel(new javax.swing.DefaultComboBoxModel<>(loadDisks()));
        srcHddRestore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                srcHddRestoreActionPerformed(evt);
            }
        });

        jLabel5.setText("Select Destination :");

        imageSource.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imageSourceActionPerformed(evt);
            }
        });

        browseImageSrc.setText("...");
        browseImageSrc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseImageSrcActionPerformed(evt);
            }
        });

        startRestore.setText("Restore");
        startRestore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startRestoreActionPerformed(evt);
            }
        });

        reloadRestore.setText("\u21bb");
        reloadRestore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reloadRestoreActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(65, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel4)
                        .addComponent(jLabel5)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(imageSource, javax.swing.GroupLayout.PREFERRED_SIZE, 459, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(browseImageSrc)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addComponent(srcHddRestore, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(reloadRestore)))
                .addGap(43, 43, 43))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(251, 251, 251)
                .addComponent(startRestore, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(111, 111, 111)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(imageSource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(browseImageSrc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 86, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(srcHddRestore, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(reloadRestore))
                .addGap(66, 66, 66)
                .addComponent(startRestore)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Restore", jPanel2);

        jLabel6.setText("Select DIsk :");

        repair.setText("Repair");
        repair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repairActionPerformed(evt);
            }
        });

        comboBoxBootRepair.setModel(new javax.swing.DefaultComboBoxModel<>(loadDisks()));

        reloadHddBootRepair.setText("\u21bb");
        reloadHddBootRepair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reloadHddBootRepairActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(55, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(comboBoxBootRepair, javax.swing.GroupLayout.PREFERRED_SIZE, 442, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(reloadHddBootRepair, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(61, 61, 61))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(246, 246, 246)
                .addComponent(repair, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(282, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxBootRepair, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(reloadHddBootRepair))
                .addGap(140, 140, 140)
                .addComponent(repair)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Boot Repair", jPanel3);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jProgressBar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addComponent(jProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
        );

        jDesktopPane2.setLayer(jPanel4, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane2Layout = new javax.swing.GroupLayout(jDesktopPane2);
        jDesktopPane2.setLayout(jDesktopPane2Layout);
        jDesktopPane2Layout.setHorizontalGroup(
            jDesktopPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jDesktopPane2Layout.setVerticalGroup(
            jDesktopPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jDesktopPane1.setLayer(jDesktopPane2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jDesktopPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDesktopPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jDesktopPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    
    private void imageSourceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_imageSourceActionPerformed
        // TODO add your handling code here:
        
        
    }//GEN-LAST:event_imageSourceActionPerformed

    private void browseImageSrcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseImageSrcActionPerformed
        // TODO add your handling code here:
        String text=fileSelector(Mode.FILE,null);
        try{
        if(!text.isEmpty()){
            imageSource.setText(text);
            startRestore.setEnabled(true);
        }
        }
        catch (NullPointerException ex){
            //Prompt.error("image Source not defined");
        }
//        if(imageSource!=null){
//            startRestore.setEnabled(true);
//        }
//        else
//        {
//            startRestore.setEnabled(false);
//        }
        
    }//GEN-LAST:event_browseImageSrcActionPerformed
    
    public static void startS(boolean flag){
        startBackup.setEnabled(flag);
        startRestore.setEnabled(flag);
        repair.setEnabled(flag);
    }
    private void startRestoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startRestoreActionPerformed
        // TODO add your handling code here:
        srcImg=imageSource.getText();
        
        startS(false);
        srcHdd =srcHdd.split(" ")[0];
        srcDir=(String)customDir.getText();
        HardDisk hardDisk = new HardDisk(srcHdd);
        try{
        srcHdd =hardDisk.getRootPartition();
        Mounter mounter = new Mounter(srcHdd);
        mounter.mount();
        
        }
        catch (Exception ex){
            
        }
        s.bashScript("python restore.py");
        //activity = new SimulatedActivity("python restore.py");
        //activity.execute();
        jProgressBar.setVisible(true);
        jProgressBar.setIndeterminate(pbf);
        
        
        imgDstHdd=(String)srcHddRestore.getSelectedItem();
        //System.out.println("hdd selected :"+imgDstHdd);
        //System.out.println("Img selected :"+srcImg);

        
    }//GEN-LAST:event_startRestoreActionPerformed

    private void srcHddRestoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_srcHddRestoreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_srcHddRestoreActionPerformed

    private void reloadHddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reloadHddActionPerformed
        disableAll();
        hdd.setModel(new javax.swing.DefaultComboBoxModel<>(loadDisks()));
        hdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hddActionPerformed(evt);
            }
        });
    }//GEN-LAST:event_reloadHddActionPerformed

    private void destinationDirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_destinationDirActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_destinationDirActionPerformed

    private void browseDestinationDirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseDestinationDirActionPerformed
        // TODO add your handling code here:

        //fileSelector(Mode.DIR,null);
        String text=fileSelector(Mode.DIR,getHomeDir());
        try{
        if(!text.isEmpty()){
            destinationDir.setText(text);
            startBackup.setEnabled(true);
        }
        }
        catch  (NullPointerException ex){
//            System.out.println("Directory field is empty");
        }
         
    }//GEN-LAST:event_browseDestinationDirActionPerformed

    private void hddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hddActionPerformed
        // TODO add your handling code here:
        //JComboBox cb = (JComboBox)evt.getSource();
        srcHdd= ((String)hdd.getSelectedItem());
        if (srcHdd!=null){
            srcHdd=srcHdd.split(" ")[0];
            System.out.println("hdd selected :"+srcHdd);
        }
        customBackup.setSelected(false);
        fullBackup.setSelected(false);

    }//GEN-LAST:event_hddActionPerformed

    private void customDirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customDirActionPerformed
        // TODO add your handling code here:
        if(customDir.getText()!=null){
            destinationDir.setEnabled(true);
            browseDestinationDir.setEnabled(true);
        }
    }//GEN-LAST:event_customDirActionPerformed

    private void fullBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fullBackupActionPerformed
        if (fullBackup.isSelected()) {
            
            if( srcHdd.isEmpty()){
                srcHdd= ((String)hdd.getSelectedItem());
                HardDisk hardDisk =new HardDisk(srcHdd);
                
            }
            
            if(!destinationDir.getText().isEmpty()){
                startBackup.setEnabled(true);
            }

            customBackup.setSelected(false);
            customDir.setEnabled(false);
            customDir.setText("");
            BrowseCustomDir.setEnabled(false);
            browseDestinationDir.setEnabled(true);
            destinationDir.setEnabled(true);

        }
    }//GEN-LAST:event_fullBackupActionPerformed

    private void startBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startBackupActionPerformed
        // TODO add your handling code here:
        startS(false);
//        activity = new SimulatedActivity("sleep 10s");
//        activity.execute();
//        jProgressBar.setVisible(true);
//        jProgressBar.setIndeterminate(pbf);
               
        dstDir =destinationDir.getText();
        srcDir =customDir.getText();
        String[] values={srcHdd,dstDir,srcDir};
        printArray(values);
        if(fullBackup.isSelected()){
            try{
                if(!dstDir.isEmpty()){
                    srcHdd =srcHdd.split(" ")[0];
                    HardDisk hardDisk = new HardDisk(srcHdd);
                    srcHdd =hardDisk.getRootPartition();
                    hardDisk.createTarImageFull(srcHdd,dstDir);
                    //hardDisk.currentRoot(srcHdd);
                    System.out.println(srcHdd+" "+dstDir);
                }
            }
            catch (InterruptedException e){
                
            }
            
            
        }
        try {
        if (customBackup.isSelected()){
            
            srcHdd =srcHdd.split(" ")[0];
            srcDir=(String)customDir.getText();
            HardDisk hardDisk = new HardDisk(srcHdd);
            srcHdd =hardDisk.getRootPartition();
            hardDisk.createTarImageCustom(srcHdd, srcDir, dstDir);
            
        }
        }
        catch (Exception e){
            
        }
        
        System.out.println("Backup completed sucessfully");
    }//GEN-LAST:event_startBackupActionPerformed

    private void BrowseCustomDirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BrowseCustomDirActionPerformed
        // TODO add your handling code here:
        // JTextField txtPath=null;
        Mounter mounter =new Mounter(rootCustom);
        try{
        mounter.mount();
        }
        catch (Exception ex){
            
        }
        String text=fileSelector(Mode.DIR,"/mnt");
        s.bashScript("baobab "+text);
        try {
        if(!text.isEmpty()){
            customDir.setText(text);
            destinationDir.setEnabled(true);
            browseDestinationDir.setEnabled(true);

        }
        }
        catch (NullPointerException ex ){
//            System.out.println("Directory field is empty");
          //  destinationDir.setEnabled(false);
        //browseDestinationDir.setEnabled(false);

        }
        
        if(!destinationDir.getText().isEmpty()){
            startBackup.setEnabled(true);
            
        }
//        destinationDir.setEnabled(true);
//        browseDestinationDir.setEnabled(true);

    }//GEN-LAST:event_BrowseCustomDirActionPerformed

    private void customBackupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_customBackupActionPerformed
        // TODO add your handling code here:
        if (customBackup.isSelected()) {

            if(srcHdd.equals("")){
                srcHdd= ((String)hdd.getSelectedItem());
                //Prompt.error("Plese select Disk");
                //customBackup.setSelected(false);
            }

            fullBackup.setSelected(false);
            startBackup.setEnabled(false);
            //destinationDir.setText(null);
            destinationDir.setEnabled(false);
            browseDestinationDir.setEnabled(false);
            HardDisk hardDisk=new HardDisk(srcHdd);
            try {
            rootCustom = hardDisk.getRootPartition();
            }
            catch (Exception ex){
                
            }
            
            //System.out.println(s.bashScript("parted -l | grep ext4"));
            //System.out.println(hardDisk.getRootPartition());
            //String cmd = " /bin/bash -c echo bluemango"+" | sudo -S parted -l | grep ext4";
            //String cmd = "mount /dev/sdb3 /home/wsmubuntu/myMount";
            //String cmd = "umount /home/wsmubuntu/myMount";
            //String cmd ="/bin/bash -c echo 'bluemango'";// | sudo -kS mkdir /home/wsmubuntu/myMount/test ls";
            printArray(hardDisk.getLinuxPartitionList());
            //System.out.println(s.runCmd(cmd));
            BrowseCustomDir.setEnabled(true);
            customDir.setEnabled(true);

        }
    }//GEN-LAST:event_customBackupActionPerformed

    private void repairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repairActionPerformed
        // TODO add your handling code here:
        String hdd =(String)comboBoxBootRepair.getSelectedItem();
    try {
        BootRepair.repair(hdd.split(" ")[0]);
        
    } catch (InterruptedException ex) {
        Logger.getLogger(MainGui.class.getName()).log(Level.SEVERE, null, ex);
    }
        
    }//GEN-LAST:event_repairActionPerformed

    private void reloadRestoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reloadRestoreActionPerformed
        // TODO add your handling code here:
        disableAll();
        srcHddRestore.setModel(new javax.swing.DefaultComboBoxModel<>(loadDisks()));
    }//GEN-LAST:event_reloadRestoreActionPerformed

    private void reloadHddBootRepairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_reloadHddBootRepairActionPerformed
        // TODO add your handling code here:
        repair.setEnabled(true);
        comboBoxBootRepair.setModel(new javax.swing.DefaultComboBoxModel<>(loadDisks()));
    }//GEN-LAST:event_reloadHddBootRepairActionPerformed
class ProgressBarFrame extends JFrame{
   
    }
    
    class SimulatedActivity extends SwingWorker<Void, Integer>
   {    String backCmd;
      /**
       * Constructs the simulated activity that increments a counter from 0 to a
       * given target.
       * @param  the target value of the counter.
       */
      public SimulatedActivity(String cmd)
      {
         backCmd=cmd;
      }
//ShellScript s = new ShellScript();
      protected Void doInBackground() throws Exception
      {
         s.bashScript(backCmd);
         switch (backCmd){
             case "backup": 
         }
         return null;
      }

      protected void process(List<Integer> chunks)
      {
         for (Integer chunk : chunks)
         {
            jProgressBar.setIndeterminate(rootPaneCheckingEnabled);
         }
      }
      
      protected void done()
      {
         startBackup.setEnabled(true);
         jProgressBar.setIndeterminate(false);
         startS(true);
      }
      
      //private int current;
      //private int target;
   } 

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws Exception {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
         //rootPass = Prompt.rootPrompt();
         //ShellScript.setRootPassword(rootPass);
         //Prompt.error("hi");
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainGui.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainGui().setVisible(true);
                
            }
        });
        
    }
      
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton BrowseCustomDir;
    public static javax.swing.JButton browseDestinationDir;
    public static javax.swing.JButton browseImageSrc;
    private javax.swing.JComboBox<String> comboBoxBootRepair;
    public static javax.swing.JRadioButton customBackup;
    public static javax.swing.JTextField customDir;
    public static javax.swing.JTextField destinationDir;
    public static javax.swing.JRadioButton fullBackup;
    public static javax.swing.JComboBox<String> hdd;
    public static javax.swing.JTextField imageSource;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JDesktopPane jDesktopPane2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    public static javax.swing.JProgressBar jProgressBar;
    private javax.swing.JTabbedPane jTabbedPane1;
    public static javax.swing.JButton reloadHdd;
    private javax.swing.JButton reloadHddBootRepair;
    public static javax.swing.JButton reloadRestore;
    public static javax.swing.JButton repair;
    public static javax.swing.JComboBox<String> srcHddRestore;
    public static javax.swing.JButton startBackup;
    public static javax.swing.JButton startRestore;
    // End of variables declaration//GEN-END:variables

}
