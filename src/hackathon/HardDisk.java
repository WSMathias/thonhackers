
package hackathon;

import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.swing.JFrame;
import javax.swing.SwingWorker;

public class HardDisk {
    static String hdd;
    public static ShellScript shell= new ShellScript();
    //static Mounter mounter;
    
    
    public  HardDisk(String hdd){
        this.hdd = hdd;
    }
    
    public  String[] getPartitionList(){
        String cmd ="lsblk /dev/"+hdd+" -lo NAME,SIZE,FSTYPE,MOUNTPOINT";
        //System.out.println(cmd);
        return shell.runCmd(cmd, 2); // line1=lable line2=disk name (sdx)
        
    }
    
    public  String[] getLinuxPartitionList(){
        String[] allPartitions=  getPartitionList();
        StringBuffer linuxPartition= new StringBuffer();
        for(int i=0;i<allPartitions.length;i++){
            String[] lineSplit =allPartitions[i].split(" ");
            if(lineSplit.length>=3){
                if(lineSplit[2].equals("ext4")||lineSplit[2].equals("ext3")){
                    linuxPartition.append(lineSplit[0]+"\n");
                }
            }
            
        }
        return linuxPartition.toString().split("\n");
    }
    
    public  boolean isRoot(String partition) throws InterruptedException{
        
        Mounter mounter =new Mounter(partition);
        String mountPoint =mounter.getMountPoint();
        mounter.mount();
        String cmd="ls "+mountPoint;
        System.out.println(cmd );
        String cmdOut = shell.runCmd(cmd);
        mounter.unMount();
        //System.out.println(cmdOut.contains("root")+ "-->in isRoot");
//        TimeUnit.SECONDS.sleep(30);
        return cmdOut.contains("root"); // && cmdOut.contains("bin") && cmdOut.contains("usr");//){
//            mounter.unMount();
//            return true;
//        }
//        else
//            mounter.unMount();
//            return false;
    }
    
    

    public  String getRootPartition() throws InterruptedException{
        String[] linuxPartition=getLinuxPartitionList();
        String rootPart=new String();
        for(int i=0; i<linuxPartition.length;i++){
            //Mounter mounter =new Mounter(linuxPartition[i]);
            if(currentRoot(linuxPartition[i])){
                rootPart = linuxPartition[i];
                break;
            }
            if(isRoot(linuxPartition[i])){
                rootPart = linuxPartition[i];
                break;
            }
        }
        return rootPart;
        
    }
    
    public void mountRoot() throws InterruptedException{
        Mounter mounter = new Mounter(getRootPartition());
        Mounter mounter1;
        //mounter1.setMountPoint(hdd);
      
        
//        mounter.mount(getRootPartition());
    }
    
    public void createFullImage(String rootPart, String imageDir){
        Mounter mounter =new Mounter(rootPart);
        String mountPint=mounter.getMountPoint();
        String cmd ="dd if=/dev/"+rootPart+" of="+imageDir+" bs=50M";
        System.out.println(cmd);
        
    }
    
    public void createTarImageCustom(String rootPart,String customDir, String imageDir){
        Mounter mounter =new Mounter(rootPart);
        String mountPint=mounter.getMountPoint();
        mounter.mount(rootPart);
        String cmd ="python tar.py "+customDir+"/backup.tar.gz"+" "+mountPint;
        SimulatedActivity sim=new SimulatedActivity(cmd);
        MainGui.jProgressBar.setVisible(true);
        MainGui.jProgressBar.setIndeterminate(true);
        sim.execute();
        mounter.unMount();
        
        System.out.println(cmd);
        
    }
    
    public void createTarImageFull(String rootPart, String imageDir){
        String mountPint="/";
        try{
            if(!rootPart.equals(" ")){      
                Mounter mounter =new Mounter(rootPart);
                mounter.mount();
                mountPint=mounter.getMountPoint();
            }
        }
        catch (Exception ex){
            mountPint="/";
        }
        
        String cmd ="python tar.py "+imageDir+"/backup.tar.gz"+" "+mountPint;
        SimulatedActivity sim=new SimulatedActivity(cmd);
        MainGui.jProgressBar.setVisible(true);
        MainGui.jProgressBar.setIndeterminate(true);
        sim.execute();
        System.out.println(cmd);
        
    }
    
    public boolean currentRoot(String partition) throws NullPointerException {
        String cmd ="lsblk /dev/"+partition+" -lo NAME,SIZE,FSTYPE,MOUNTPOINT";
        String[] out =shell.runCmd(cmd).split("\n")[1].replaceAll(" +", " ").split(" ");
        String root="";
        if(out.length>=4){
            System.out.println(out[0]);
            root=out[3];
        }
        
        if(out.equals("/")){
            System.out.println("current root is at:"+root);
            return true;
        }
        else{
            return false;
        }
    }
}

class ProgressBarFrame extends JFrame{
   
    }
    
    class SimulatedActivity extends SwingWorker<Void, Integer>
   {    String backCmd;
        ShellScript s =new ShellScript();
      /**
       * Constructs the simulated activity that increments a counter from 0 to a
       * given target.
       * @param  the target value of the counter.
       */
      public SimulatedActivity(String cmd)
      {
         backCmd=cmd;
      }
//ShellScript s = new ShellScript();
      protected Void doInBackground() throws Exception
      {
         s.runCmd(backCmd);
         return null;
      }

      protected void process(List<Integer> chunks)
      {
         for (Integer chunk : chunks)
         {
            MainGui.jProgressBar.setIndeterminate(true);
         }
      }
      
      protected void done()
      {
         //startBackup.setEnabled(true);
         MainGui.jProgressBar.setIndeterminate(false);
         MainGui.startS(true);
      }
      
      //private int current;
      //private int target;
   } 

