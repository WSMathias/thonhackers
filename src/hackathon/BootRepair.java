
package hackathon;

public class BootRepair {
    DiskTool disk= new DiskTool();
    
    public static void repair(String hdd) throws InterruptedException{
        System.out.println("running boot repair on :"+hdd);
        HardDisk hardDisk =new HardDisk(hdd);;
        String root = hardDisk.getRootPartition();
        System.out.println("root is in :"+root);
        Mounter mounter =new Mounter(root);
        mounter.mount();
        mounter.mountBinder();
        ShellScript shell=new ShellScript();
        shell.runCmd("python bootfix.py");
        mounter.mountUnBinder();
        mounter.unMount();
        System.out.println("boot loader recovered sucessfully");
    }
    
    
}
