
package hackathon;

public class Mounter {
    private String mountPoint ="/mnt";
//    private String mountPoint ="/home/wsmubuntu/myMount";
    private String partition =new String();
    
    public Mounter(String partition){
        this.partition =partition;
    }
    
    
    //mount the given partition on default location
    public boolean mount(String partition){
        ShellScript s=new ShellScript();
        String cmd="mount /dev/"+partition+" "+mountPoint;
        if(s.runCmd(cmd)==null){
        return true;
        }
        else{
            return false;
        }
    }
    
    //mount the object partition on default location
    public boolean mount() throws InterruptedException{
        ShellScript s=new ShellScript();
        String cmd="mount /dev/"+partition+" "+mountPoint;
        System.out.println(cmd);
        if(s.runCmd(cmd).length()==0){
            //Thread.sleep(3000);
            System.out.println(partition +" mounted at: "+mountPoint);
            return true;
        }
        else{
            System.out.println("failed to mount:"+partition);
            return false;
        }
        //return true;
    }
    
    public boolean mount(String src,String mountPoint){
        ShellScript s=new ShellScript();
        this.mountPoint = mountPoint;
        String cmd="mount /dev/"+src+" "+mountPoint;
        if(s.runCmd(cmd)==null){
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean unMount(){
        ShellScript s=new ShellScript();
        this.mountPoint = mountPoint;
        String cmd="umount "+mountPoint;
        System.out.println(cmd);
        if(s.runCmd(cmd)==null){
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean mountBinder(String mountPoint){
        ShellScript s=new ShellScript();
        setMountPoint(mountPoint);
        String cmd ="mount --bind /dev "+mountPoint+"/dev;" + "mount --bind /sys "+mountPoint+"/sys;" + "mount --bind /proc "+mountPoint+"/proc;";
        if(s.runCmd(cmd).length()==0){
            return true;
        }
        else{
            return false;
        }
    }
    
    public boolean mountBinder(){
        ShellScript s=new ShellScript();
        //setMountPoint(mountPoint);
//        String cmdDev ="mount --bind /dev "+mountPoint+"/dev";
//        String cmdSys ="mount --bind /sys "+mountPoint+"/dev"; 
//        String cmdProc ="mount --bind /proc "+mountPoint+"/dev"; 
//        s.runCmd(cmdDev);
//        s.runCmd(cmdSys);
//        s.runCmd(cmdProc);
//        System.out.println(cmdDev);
            s.runCmd("python bind.py "+mountPoint);
//        if(s.runCmd(cmd).length()==0){
//            return true;
//        }
//        else{
//            return false;
//        }
            return true;
    }
    

    
    public boolean mountUnBinder(){
        ShellScript s=new ShellScript();
//        String cmd ="umount "+mountPoint+"/dev;" + "umount "+mountPoint+"/sys;" + "mount "+mountPoint+"/proc;";
//        if(s.runCmd(cmd)==null){
//            return true;
//        }
//        else{
//            return false;
//        }
            s.runCmd("python unbind.py "+mountPoint);
            return true;
    }
    
    public void setMountPoint(String mountPoint){
        
        this.mountPoint = mountPoint;
        
    }
    
    public String getMountPoint(){
        return mountPoint;
    }
       
    
}
